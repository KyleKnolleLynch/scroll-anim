import type { Metadata } from 'next'
import { Public_Sans } from 'next/font/google'
import './globals.css'

const publicSans = Public_Sans({ subsets: ['latin'] })

export const metadata: Metadata = {
    title: 'Scroll Image Animation',
    description: 'Sandbox to try out image animation on scroll with threejs',
}

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode
}>) {
    return (
        <html lang='en'>
            <body className={publicSans.className}>{children}</body>
        </html>
    )
}
