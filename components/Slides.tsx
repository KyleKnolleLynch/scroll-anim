'use client'

import { ImageProps, Image as DImage, useScroll } from '@react-three/drei'
import { useThree, useFrame } from '@react-three/fiber'
import { useRef } from 'react'
import { Group, MathUtils } from 'three'

function DreiImage(props: ImageProps) {
    const ref = useRef(null)
    const group = useRef<Group>(null)
    const data = useScroll()

    useFrame((state, delta) => {
        if (group.current && ref.current && data) {
            group.current.position.z = MathUtils.damp(
                group.current.position.z,
                Math.max(0, data.delta * 100),
                4,
                delta
            )

            //  @ts-ignore
            ref.current.material.grayscale = MathUtils.damp(
                //  @ts-ignore
                ref.current.material.grayscale,
                Math.max(0, 1 - data.delta * 1000),
                4,
                delta
            )
        }
    })

    return (
        <group ref={group}>
            <DImage ref={ref} {...props} />
        </group>
    )
}

function Slide({ urls = [''], ...props }) {
    const ref = useRef(null)
    const { width } = useThree(state => state.viewport)
    const w = width < 10 ? 1.5 / 3 : 1 / 3

    return (
        <group ref={ref} {...props}>
            <DreiImage
                position={[-width * w, 0, 0]}
                scale={[5, 7]}
                url={urls[0]}
            />
            <DreiImage position={[0, 0, 0]} scale={[7, 5]} url={urls[1]} />
            <DreiImage
                position={[width * w, 0, 0]}
                scale={[4, 6]}
                url={urls[2]}
            />
        </group>
    )
}

export default function Slides() {
    const { width } = useThree(state => state.viewport)
    return (
        <>
            <Slide
                position={[0, 0, 0]}
                urls={[
                    'https://images.unsplash.com/photo-1511497584788-876760111969?q=80&w=1632&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1497752531616-c3afd9760a11?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1547140741-00d6fd251528?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ]}
            />
            <Slide
                position={[width * 1, 0, 0]}
                urls={[
                    'https://images.unsplash.com/photo-1511497584788-876760111969?q=80&w=1632&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1497752531616-c3afd9760a11?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1547140741-00d6fd251528?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ]}
            />
            <Slide
                position={[width * 2, 0, 0]}
                urls={[
                    'https://images.unsplash.com/photo-1511497584788-876760111969?q=80&w=1632&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1497752531616-c3afd9760a11?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                    'https://images.unsplash.com/photo-1547140741-00d6fd251528?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
                ]}
            />
        </>
    )
}
