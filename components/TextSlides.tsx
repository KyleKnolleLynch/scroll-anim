export default function TextSlides() {
    return (
        <section className='flex'>
            <p className='absolute top-[20vh] left-[25vw] text-[12vw] font-black'>
                Travel
            </p>
            <p className='absolute top-[20vh] left-[125vw] text-[12vw] font-black'>
                Explore
            </p>
            <p className='absolute top-[20vh] left-[225vw] text-[12vw] font-black'>
                Adventure
            </p>
        </section>
    )
}
